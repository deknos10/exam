import logging
import sys

# Указываем формат логов
access_format = logging.Formatter(
    '(%(asctime)s), %(message)s'
)
system_format = logging.Formatter(
    '%(asctime)s - %(levelname)s - %(message)s'
)

# Ставим логгер
def setup_logger(name, stream, formatter, level=logging.INFO):
    handler = logging.StreamHandler(stream)
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger


access_logger = setup_logger(
    'access_logger',
    sys.stdout,
    access_format
)
error_logger = setup_logger(
    'error_logger',
    sys.stderr,
    access_format,
    level=logging.ERROR
)
system_logger = setup_logger(
    'system_error',
    sys.stderr,
    system_format,
    level=logging.ERROR
)
